/// <reference path="../../typings/index.d.ts" />

"use strict";

import * as express from 'express';
import * as Knex from 'knex';
import * as path from 'path';
import * as fs from 'fs';

//set up table and primary index field names
const DBSuccessCode:number = 200;
const DBErrorCode:number = 400;
var projectdir = path.join(__dirname,'../../');

//get configuration info
var filecontents=fs.readFileSync(path.join(projectdir,'TruckLoading.cfg'),{encoding:'utf8'});
console.log('filecontents:',filecontents);
var cfg=JSON.parse(filecontents);
var tableName=cfg.tableName;
var keyFieldName=cfg.keyFieldName;

//set up query builder and database connection pool
var knex=Knex({client: 'mysql', connection: cfg})

export function getSingleTableOptions(req:express.Request, res:express.Response){
    console.log('getSingleTableOptions');
    res.status(DBSuccessCode).send('GET PUT DELETE POST OPTIONS');
}

export function getSingleTableList(req:express.Request, res:express.Response){
    console.log('getSingleTableList:', tableName);
    knex
        .select()
        .from(tableName)
        .orderBy('idtruckloads')
        .then(data => {
            console.log('results', data);
            res.status(DBSuccessCode).json(data); })
        .catch(err =>{res.status(DBErrorCode).send(err)})
    ;
}

export function getSingleTableId(req:express.Request, res:express.Response){
    console.log('getSingleTableId');
    knex
        .select()
        .from(tableName)
        .where(keyFieldName, req.params['id'])
            .then(data => {res.status(DBSuccessCode).json(data);})
            .catch(err => {res.status(DBErrorCode).send(err)})
        ;
}

export function getSingleTableRange(req:express.Request, res:express.Response){
    console.log('getSingleTableRange');
    knex
        .select()
        .from(tableName)
        .offset(parseInt(req.params['offset']))
        .limit(parseInt(req.params['limit']))
        .then(data => {res.status(DBSuccessCode).json(data);})
        .catch(err => {res.status(DBErrorCode).send(err)})
        ;

}

export function addSingleTableEntry(req:express.Request, res:express.Response){
    console.log('addSingleTableEntry', req.body);
    req.body.timestamp='2020-01-01';
    knex
        .into(tableName)
        .insert(req.body)
        .then(data => {res.status(DBSuccessCode).json(data);})
        .catch(err => {
            console.log('addSingleTableEntry error:', err);
            res.status(DBErrorCode).send(err);})
    ;
}

export function updateSingleTableEntry(req:express.Request, res:express.Response) {
    console.log('updateSingleTableEntry', req.body);
    knex
        .update(req.body)
        .from(tableName)
        .where(keyFieldName, req.body.idtruckloads)
        .then(data => {res.status(DBSuccessCode).json(data);})
        .catch(err => {
            console.log('update error:',err);
            res.status(DBErrorCode).send(err)})
    ;
}

export function deleteSingleTableEntry(req:express.Request, res:express.Response){
    console.log('deleteSingleTableEntry', req.body);
    knex
        .delete()
        .from(tableName)
        .where(keyFieldName, req.body.idtruckloads)
        .then(data => {res.status(DBSuccessCode).json(data);})
        .catch(err => {res.status(DBErrorCode).send(err)})
    ;
}